class Car < ApplicationRecord
  validates :pick_up_location, presence: true, length: { minimum: 3 }
  validates :drop_off_location, presence: true, length: { minimum: 3 }
  validates :pick_up_date, presence: true
  validates :pick_up_hour, presence: true
  validates :drop_off_date, presence: true
  validates :drop_off_hour, presence: true
end
