class Hotel < ApplicationRecord
  validates :where, presence: true, length: { minimum: 3 }
  validates :check_in, presence: true
  validates :check_out, presence: true
  validates :rooms, presence: true
  validates :guests, presence: true
end
