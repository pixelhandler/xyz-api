class Api::V1::HotelResource < JSONAPI::Resource
  attributes :where, :check_in, :check_out, :rooms, :guests
end
