class Api::V1::CarResource < JSONAPI::Resource
  attributes :pick_up_location, :drop_off_location, :pick_up_date, :pick_up_hour, :drop_off_date, :drop_off_hour
end
