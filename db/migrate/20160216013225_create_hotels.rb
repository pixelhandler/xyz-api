class CreateHotels < ActiveRecord::Migration[5.0]
  def change
    create_table :hotels do |t|
      t.string :where
      t.date :check_in
      t.date :check_out
      t.integer :rooms
      t.integer :guests

      t.timestamps
    end
  end
end
