class CreateCars < ActiveRecord::Migration[5.0]
  def change
    create_table :cars do |t|
      t.string :pick_up_location
      t.string :drop_off_location
      t.date :pick_up_date
      t.string :pick_up_hour
      t.date :drop_off_date
      t.string :drop_off_hour

      t.timestamps
    end
  end
end
