Rails.application.routes.draw do

  namespace :api do
    namespace :v1 do
      jsonapi_resources :hotels
      jsonapi_resources :cars
    end
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
